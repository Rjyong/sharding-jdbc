package com.example.shardingjdbc.mapper;

import com.example.shardingjdbc.entity.User;

/**
 * Created by 5566 on 2019/7/24.
 * aYong
 */
public interface UserMapper {

    void insert(User user);

    User findById(Long id);
}
