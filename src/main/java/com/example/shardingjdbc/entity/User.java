package com.example.shardingjdbc.entity;

import lombok.Data;

/**
 * Created by 5566 on 2019/7/24.
 * aYong
 */
@Data
public class User {

    private Long id;
    private Long order_id;
    private Long user_id;
    private String userName;
    private String passWord;
    private String nick_name;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", order_id=" + order_id +
                ", user_id=" + user_id +
                ", userName='" + userName + '\'' +
                ", passWord='" + passWord + '\'' +
                ", nick_name='" + nick_name + '\'' +
                '}';
    }
}
