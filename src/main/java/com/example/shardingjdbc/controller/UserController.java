package com.example.shardingjdbc.controller;

import com.example.shardingjdbc.entity.User;
import com.example.shardingjdbc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 5566 on 2019/7/24.
 * aYong
 */
@Service
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 测试新增
     * @param id
     * @param user_id
     * @param order_id
     * @param nickName
     * @param passWord
     * @param userName
     * @return
     * http://localhost:8080/update1?id=1&user_id=1&order_id=1&nickName=%E5%BC%A0%E4%B8%89&passWord=123456&userName=%E7%94%A8%E6%88%B71
     */
    @RequestMapping(value="update1")
    public String updateTransactional(@RequestParam(value = "id") Long id,
                                      @RequestParam(value = "user_id") Long user_id,
                                      @RequestParam(value = "order_id") Long order_id,
                                      @RequestParam(value = "nickName") String nickName,
                                      @RequestParam(value = "passWord") String passWord,
                                      @RequestParam(value = "userName") String userName
    ) {
        User user2 = new User();
        user2.setId(id);
        user2.setUser_id(user_id);
        user2.setOrder_id(order_id);
        user2.setNick_name(nickName);
        user2.setPassWord(passWord);
        user2.setUserName(userName);
        userService.insert(user2);
        return "success";
    }

    /**
     * 测试查询
     * @param id
     * @return
     */
    @RequestMapping(value="findById")
    public String updateTransactional(@RequestParam(value = "id") Long id
    ) {
        User user = userService.findById(id);
        return user.toString();
    }
}
